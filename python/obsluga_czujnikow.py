import sys
import math

from sense_hat import SenseHat
sense = SenseHat()

def computeHeight(press = 7):

	return (math.log(press/1013.25)*8.31*(7+273.15))/(-0.028*9.8)
	
if (sys.argv[1] == "Polozenie"):
	orientation = sense.get_orientation()
	print("p: {pitch}, r: {roll}, y: {yaw}".format(**orientation))
elif (sys.argv[1] == "Cisnienie"):
	press = sense.get_pressure()
	temp = sense.get_temperature_from_pressure()
	print("Cisnienie: %f hPa, Wysokosc: %f m" %(press, computeHeight(press)))
elif (sys.argv[1] == "Temperatura"):
	temp = sense.get_temperature_from_pressure()
	humidity = sense.get_humidity()
	print("Temperatura: %f &#176C, Wilgotnosc: %f &#37" %(temp, humidity))